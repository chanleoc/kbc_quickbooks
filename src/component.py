'''
"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2017'"
"__project__ = 'kbc_quickbooks'"

'''

import logging

from keboola.component.base import ComponentBase

from mapping import mapping
from quickbooks import quickbooks
from report_mapping import report_mapping  # noqa

# configuration variables
KEY_COMPANY_ID = 'companyid'
KEY_ENDPOINT = 'endpoints'
KEY_LOAD_TYPE = 'load_type'

# list of mandatory parameters => if some is missing,
# component will fail with readable message on initialization.
REQUIRED_PARAMETERS = []
REQUIRED_IMAGE_PARS = []

# QuickBooks Parameters
BASE_URL = "https://quickbooks.api.intuit.com"

# destination to fetch and output files
DEFAULT_FILE_INPUT = "/data/in/tables/"
DEFAULT_FILE_DESTINATION = "/data/out/tables/"

# Component Version
logging.info("Quickbooks Version: {0}".format("0.2.8"))


class Component(ComponentBase):
    """
        Extends base class for general Python components. Initializes the CommonInterface
        and performs configuration validation.

        For easier debugging the data folder is picked up by default from `../data` path,
        relative to working directory.

        If `debug` parameter is present in the `config.json`, the default logger is set to verbose DEBUG mode.
    """

    def __init__(self):
        super().__init__()

        try:
            # validation of required parameters. Produces ValueError
            self.validate_configuration(REQUIRED_PARAMETERS)
            self.validate_image_parameters(REQUIRED_IMAGE_PARS)
        except ValueError as e:
            logging.exception(e)
            exit(1)

    def run(self):
        '''
        Main execution code
        '''

        p = self.configuration.parameters
        params = p['config']

        # Input parameters
        endpoints = params.get(KEY_ENDPOINT)
        company_id = params.get(KEY_COMPANY_ID)
        load_type = params.get(KEY_LOAD_TYPE)
        incremental = False if load_type == 'full_load' else True
        logging.info(f'Company ID: {company_id}')

        # INITALIZING QUICKBOOKS INSTANCES
        oauth = self.configuration.oauth_credentials
        quickbooks_param = quickbooks(company_id=company_id, oauth=oauth)

        # Fetching reports for each configured endpoint
        for endpt in endpoints:
            # Endpoint parameters
            if "**" in endpt["endpoint"]:
                endpoint = endpt["endpoint"].split("**")[0]
                report_api_bool = True
            else:
                endpoint = endpt["endpoint"]
                report_api_bool = False

            # Phase 1: Request
            # Handling Quickbooks Requests
            quickbooks_param.fetch(
                endpoint=endpoint,
                report_api_bool=report_api_bool,
                start_date=endpt["start_date"],
                end_date=endpt["end_date"]
            )

            # Phase 2: Mapping
            # Translate Input JSON file into CSV with configured mapping
            # For different accounting_type,
            # input_data will be outputting Accrual Type
            # input_data_2 will be outputting Cash Type
            logging.info("Parsing API results...")
            input_data = quickbooks_param.data

            # if there are no data
            # output blank
            if len(input_data) == 0:
                pass

            else:
                logging.info(
                    "Report API Template Enable: {0}".format(report_api_bool))

                if report_api_bool:

                    if endpoint == "CustomQuery":
                        report_mapping(endpoint=endpoint, data=input_data,
                                       query=endpt["start_date"])

                    else:
                        if endpoint in quickbooks_param.reports_required_accounting_type:
                            input_data_2 = quickbooks_param.data_2

                            report_mapping(
                                endpoint=endpoint, data=input_data, accounting_type="accrual")
                            report_mapping(
                                endpoint=endpoint, data=input_data_2, accounting_type="cash")

                        else:
                            report_mapping(endpoint=endpoint, data=input_data)
                else:
                    mapping(endpoint=endpoint, data=input_data)


def flatten_json(y):
    """
    # Credits: https://gist.github.com/amirziai/2808d06f59a38138fa2d
    # flat out the json objects
    """
    out = {}

    def flatten(x, name=''):
        if type(x) is dict:
            for a in x:
                flatten(x[a], name + a + '/')
        elif type(x) is list:
            i = 0
            for a in x:
                flatten(a, name + str(i) + '/')
                i += 1
        else:
            out[name[:-1]] = x

    flatten(y)

    return out


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(2)
