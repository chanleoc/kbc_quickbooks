# Terms and Conditions

The QuickBooks Extractor for KBC is built and offered by Leo as a third party component. It is provided as-is, without guarantees and support, and for no additional charge. 
Component's task is to help user to extract the data from QuickBooks online to Keboola Connection Platform (KBC). 
API call is process by using user-entered keys for API authentication, no sensitive information is being sent non-standard way, maintaining all Keboola recommended security standards along the way.

## QuickBooks licence Terms
[Official website](https://community.intuit.com/questions/1379927)

## Contact

Leo  
Vancouver, Canada (PST time)  
email: support@keboola.com  