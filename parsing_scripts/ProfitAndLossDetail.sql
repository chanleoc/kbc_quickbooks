------------------------------------------------------------------------
-- FIRST SECTION
CREATE OR REPLACE TABLE "stage_1" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM
    "ProfitAndLossDetail_accrual"
;

CREATE OR REPLACE TABLE "stage_1_flatten" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_1"
    ,VALUE as "value"
FROM "stage_1",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
;

------------------------------------------------------------------------
-- SECOND SECTION
-- FILTERING OUT THE SUMMARY SECTIONS
CREATE OR REPLACE TABLE "stage_2" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_1_flatten"
WHERE obj_qbo_table IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_2_flatten" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_2"
    ,VALUE as "value"
FROM "stage_2",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
;

------------------------------------------------------------------------
-- THIRD SECTION
CREATE OR REPLACE TABLE "stage_3" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_2_flatten"
;

CREATE OR REPLACE TABLE "stage_3_flatten" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_3"
    ,VALUE as "value"
FROM "stage_3",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_3" IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_3_DATA" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_3"
    ,VALUE:ColData[0].value::DATE AS "tx_date"
    ,VALUE:ColData[1].value::VARCHAR AS "txn_type"
    ,VALUE:ColData[2].value::VARCHAR AS "doc_num"
    ,VALUE:ColData[3].value::VARCHAR AS "name"
    ,VALUE:ColData[4].value::VARCHAR AS "dept_name"
    ,VALUE:ColData[5].value::VARCHAR AS "klass_name"
    ,VALUE:ColData[6].value::VARCHAR AS "memo"
    ,VALUE:ColData[7].value::VARCHAR AS "split_acc"
    ,VALUE:ColData[8].value::VARCHAR AS "subt_nat_amount"
    ,VALUE:ColData[8].value::VARCHAR AS "rbal_nat_amount"
FROM "stage_3",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_3" IS NULL
;

------------------------------------------------------------------------
-- FOURTH SECTION
CREATE OR REPLACE TABLE "stage_4" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_3_flatten"
;

CREATE OR REPLACE TABLE "stage_4_flatten" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_4"
    ,VALUE as "value"
FROM "stage_4",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_4" IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_4_DATA" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_4"
    ,VALUE:ColData[0].value::DATE AS "tx_date"
    ,VALUE:ColData[1].value::VARCHAR AS "txn_type"
    ,VALUE:ColData[2].value::VARCHAR AS "doc_num"
    ,VALUE:ColData[3].value::VARCHAR AS "name"
    ,VALUE:ColData[4].value::VARCHAR AS "dept_name"
    ,VALUE:ColData[5].value::VARCHAR AS "klass_name"
    ,VALUE:ColData[6].value::VARCHAR AS "memo"
    ,VALUE:ColData[7].value::VARCHAR AS "split_acc"
    ,VALUE:ColData[8].value::VARCHAR AS "subt_nat_amount"
    ,VALUE:ColData[8].value::VARCHAR AS "rbal_nat_amount"
FROM "stage_4",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_4" IS NULL
;

------------------------------------------------------------------------
-- FIFTH SECTION
CREATE OR REPLACE TABLE "stage_5" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_4_flatten"
;

CREATE OR REPLACE TABLE "stage_5_flatten" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_5"
    ,VALUE as "value"
FROM "stage_5",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_5" IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_5_DATA" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_5"
    ,VALUE:ColData[0].value::DATE AS "tx_date"
    ,VALUE:ColData[1].value::VARCHAR AS "txn_type"
    ,VALUE:ColData[2].value::VARCHAR AS "doc_num"
    ,VALUE:ColData[3].value::VARCHAR AS "name"
    ,VALUE:ColData[4].value::VARCHAR AS "dept_name"
    ,VALUE:ColData[5].value::VARCHAR AS "klass_name"
    ,VALUE:ColData[6].value::VARCHAR AS "memo"
    ,VALUE:ColData[7].value::VARCHAR AS "split_acc"
    ,VALUE:ColData[8].value::VARCHAR AS "subt_nat_amount"
    ,VALUE:ColData[8].value::VARCHAR AS "rbal_nat_amount"
FROM "stage_5",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_5" IS NULL
;

------------------------------------------------------------------------
-- SIX SECTION
CREATE OR REPLACE TABLE "stage_6" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_5_flatten"
;

CREATE OR REPLACE TABLE "stage_6_flatten" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_6"
    ,VALUE as "value"
FROM "stage_6",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_6" IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_6_DATA" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_6"
    ,VALUE:ColData[0].value::DATE AS "tx_date"
    ,VALUE:ColData[1].value::VARCHAR AS "txn_type"
    ,VALUE:ColData[2].value::VARCHAR AS "doc_num"
    ,VALUE:ColData[3].value::VARCHAR AS "name"
    ,VALUE:ColData[4].value::VARCHAR AS "dept_name"
    ,VALUE:ColData[5].value::VARCHAR AS "klass_name"
    ,VALUE:ColData[6].value::VARCHAR AS "memo"
    ,VALUE:ColData[7].value::VARCHAR AS "split_acc"
    ,VALUE:ColData[8].value::VARCHAR AS "subt_nat_amount"
    ,VALUE:ColData[8].value::VARCHAR AS "rbal_nat_amount"
FROM "stage_6",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_6" IS NULL
;

------------------------------------------------------------------------
-- 7th SECTION
CREATE OR REPLACE TABLE "stage_7" AS
SELECT
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,"Section_6"
    ,parse_json("value"):Rows AS obj_qbo_table
FROM "stage_6_flatten"
;

CREATE OR REPLACE TABLE "stage_7_flatten" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,"Section_6"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_7"
    ,VALUE as "value"
FROM "stage_7",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_7" IS NOT NULL
;

CREATE OR REPLACE TABLE "stage_7_DATA" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,"Section_6"
    ,VALUE:Header:ColData[0]:value::VARCHAR AS "Section_7"
    ,VALUE:ColData[0].value::DATE AS "tx_date"
    ,VALUE:ColData[1].value::VARCHAR AS "txn_type"
    ,VALUE:ColData[2].value::VARCHAR AS "doc_num"
    ,VALUE:ColData[3].value::VARCHAR AS "name"
    ,VALUE:ColData[4].value::VARCHAR AS "dept_name"
    ,VALUE:ColData[5].value::VARCHAR AS "klass_name"
    ,VALUE:ColData[6].value::VARCHAR AS "memo"
    ,VALUE:ColData[7].value::VARCHAR AS "split_acc"
    ,VALUE:ColData[8].value::VARCHAR AS "subt_nat_amount"
    ,VALUE:ColData[8].value::VARCHAR AS "rbal_nat_amount"
FROM "stage_7",
    LATERAL FLATTEN (INPUT => obj_qbo_table:Row) f
WHERE "Section_7" IS NULL
;

------------------------------------------------------------------------
-- CONCATENATING ALL DATA
CREATE OR REPLACE TABLE "stg01_ProfitAndLossDetail_Accrual" AS
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,'' AS "Section_3"
    ,'' AS "Section_4"
    ,'' AS "Section_5"
    ,'' AS "Section_6"
    ,"tx_date"
    ,"txn_type"
    ,"doc_num"
    ,"name"
    ,"dept_name"
    ,"klass_name"
    ,"memo"
    ,"split_acc"
    ,"subt_nat_amount"
    ,"rbal_nat_amount"
FROM "stage_3_DATA"
UNION
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,'' AS "Section_4"
    ,'' AS "Section_5"
    ,'' AS "Section_6"
    ,"tx_date"
    ,"txn_type"
    ,"doc_num"
    ,"name"
    ,"dept_name"
    ,"klass_name"
    ,"memo"
    ,"split_acc"
    ,"subt_nat_amount"
    ,"rbal_nat_amount"
FROM "stage_4_DATA"
UNION
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,'' AS "Section_5"
    ,'' AS "Section_6"
    ,"tx_date"
    ,"txn_type"
    ,"doc_num"
    ,"name"
    ,"dept_name"
    ,"klass_name"
    ,"memo"
    ,"split_acc"
    ,"subt_nat_amount"
    ,"rbal_nat_amount"
FROM "stage_5_DATA"
UNION
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,'' AS "Section_6"
    ,"tx_date"
    ,"txn_type"
    ,"doc_num"
    ,"name"
    ,"dept_name"
    ,"klass_name"
    ,"memo"
    ,"split_acc"
    ,"subt_nat_amount"
    ,"rbal_nat_amount"
FROM "stage_6_DATA"
UNION
SELECT 
    "StartPeriod"
    ,"EndPeriod"
    ,"Section_1"
    ,"Section_2"
    ,"Section_3"
    ,"Section_4"
    ,"Section_5"
    ,"Section_6"
    ,"tx_date"
    ,"txn_type"
    ,"doc_num"
    ,"name"
    ,"dept_name"
    ,"klass_name"
    ,"memo"
    ,"split_acc"
    ,"subt_nat_amount"
    ,"rbal_nat_amount"
FROM "stage_7_DATA"
;